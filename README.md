# GameRand

Collection of some RNGs found in games.

## Wishlist

- [x] Super Mario 64 RNG
- [ ] Super Mario World RNG
- [x] Pokémon Mystery Dungeon (the 1st game) RNG
- [x] Ultimate Doom RNG
- [ ] Integrade with the `rand` crate (if possible)
- [ ] Provide a miminal binary that runs/demonstrates
  - [x] Ability to benchmark certain RNGs
  - [ ] Ability to get example values from certain RNGs
