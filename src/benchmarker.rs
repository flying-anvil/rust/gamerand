use std::time::Instant;

pub fn benchmark<F>(name: &str, iterations: usize, mut generate_function: F)
where
    F: FnMut() -> (),
{
    let start = Instant::now();

    println!("Running \x1B[4;1m{name}\x1B[0m with {} iterations", pretty_print_int(iterations, '_'));

    for _ in 0..iterations {
        generate_function();
    }

    let duration = start.elapsed();

    println!(
        "took {:.1}ms | {:0.3} ns per rng | {} rng per second",
        duration.as_micros() as f32 / 1e3,
        duration.as_nanos() as f64 / iterations as f64,
        convert_with_unit(iterations as f32 / duration.as_secs_f32()),
    );
}

fn convert_with_unit(number: f32) -> String {
    let mut order_of_magnitude = 0;

    let mut i = number;
    while i > 1000. {
        i /= 1000.;
        order_of_magnitude += 1;
    }

    let suffix = match order_of_magnitude {
        0 => "",
        1 => "k",
        2 => "M",
        3 => "G",
        4 => "T",
        5 => "P",
        6 => "E",
        _ => unreachable!(),
    };

    format!("{i:0.3}{suffix}")
}

fn pretty_print_int(number: usize, separator: char) -> String {
    let number_as_string = number.to_string();
    let capacity = number_as_string.len() + (((number_as_string.len() - 1) as f32 / 3.) as usize);
    let mut result = String::with_capacity(capacity);

    // Start from behind and push chars to result, with each 3rd char being prepended by the separator.
    // Prepend new chars because we are iterating in reverse.
    number_as_string.chars().rev().enumerate().for_each(|(index, char)| {
        if index != 0 && index % 3 == 0 {
            result.insert(0, separator);
        }

        result.insert(0, char);
    });

    result
}
