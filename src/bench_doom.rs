use crate::benchmarker::benchmark;
use gamerand::rng::doom::Doom;

pub fn bench_doom() {
    let mut doom = Doom::new();
    let iterations = 1_000_000_000;

    benchmark("Doom", iterations, || {
        std::hint::black_box(doom.next());
    });
}
