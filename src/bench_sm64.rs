use crate::benchmarker::benchmark;
use gamerand::rng::sm64::SM64;

pub fn bench_sm64() {
    let mut sm64 = SM64::new();
    let iterations = 1_000_000_000;

    benchmark("SM64", iterations, || {
        std::hint::black_box(sm64.next());
    });
}
