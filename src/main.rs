use bench_pmd1::bench_pmd1;
use bench_sm64::bench_sm64;
use bench_doom::bench_doom;

mod benchmarker;
mod bench_sm64;
mod bench_pmd1;
mod bench_doom;

const RNG_LIST: [&'static str; 3] = [
    "sm64",
    "pmd1",
    "doom",
];

fn main() {
    let mut args = std::env::args();
    let executable = args.next().unwrap_or_else(|| "".to_string());
    let rng = args.next().unwrap_or_else(|| "".to_string()).to_lowercase();

    match rng.as_ref() {
        "--help" | "-h" | "" => print_help(&executable),

        "--version" | "-v" => println!(env!("CARGO_PKG_VERSION")),

        "sm64" => bench_sm64(),
        "pmd1" => bench_pmd1(),
        "doom" => bench_doom(),

        "all" => {
            bench_sm64();
            println!("");
            bench_pmd1();
            println!("");
            bench_doom();
        },

        invalid => {
            eprintln!("RNG {invalid} does not exist");
            print_help(&executable);
        }
    }
}

fn print_help(executable: &str) {
    print!(
        concat!(
            "Usage: {} $rng\n",
            "Where $rng is one of the following: \x1B[1mall\x1B[0m, {}\n",
        ),
        executable,
        RNG_LIST.iter()
            .map(|rng| format!("\x1B[1m{rng}\x1B[0m"))
            .collect::<Vec<String>>()
            .join(", "),
    )
}
