use crate::benchmarker::benchmark;
use gamerand::rng::pmd1::{PMD1Dungeon, PMD1Primary};

pub fn bench_pmd1() {
    let mut pmd1_dungeon = PMD1Dungeon::new();
    let iterations = 1_000_000_000;

    benchmark("PMD1 Dungeon", iterations, || {
        std::hint::black_box(pmd1_dungeon.next());
    });

    println!("");

    let mut pmd1_primary = PMD1Primary::new();
    let iterations = 1_000_000_000;

    benchmark("PMD1 Primary", iterations, || {
        std::hint::black_box(pmd1_primary.next());
    });
}
