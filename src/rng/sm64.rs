pub struct SM64 {
    seed: u32,
}

impl SM64 {
    pub fn new() -> Self {Self { seed: 0 }}
    pub fn new_seeded(seed: u32) -> Self {Self { seed }}

    pub fn next(&mut self) -> u32 {
        self.seed = Self::generate(self.seed);

        return self.seed;
    }

    pub fn generate(seed: u32) -> u32 {
        // This if breaks the 2-cycle between 22026 <=> 58704 (which should never be reached)
        let mut result = if seed == 0x560A { 0 } else { seed };

        let mut s0: u32 = (((result as u8) as u32) << 8) as u32;
        s0 = s0 ^ result;
        result = ((s0 & 0xFF) << 8) | ((s0 & 0xFF00) >> 8);
        s0 = (s0 << 1) ^ result;

        let s1: u32 = (s0 >> 1) ^ 0xFF80;

        if s0 & 1 == 0 {
            // This is the unneccessary early-loop
            result = if s1 == 0xAA55 { 0 } else { s1 ^ 0x1FF4 }
        } else {
            result = s1 ^ 0x8180;
        }

        result
    }
}
