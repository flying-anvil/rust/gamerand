/// This RNG is used to seed the primary RNG
pub struct PMD1Dungeon {
    seed: u32,
}

const MAGIC_NUMBER: u32 = 1566083941;

impl PMD1Dungeon {
    pub fn new() -> Self {Self { seed: 0 }}

    // Prevent seed from being 0 + cut it off
    pub fn new_seeded(seed: u32) -> Self {Self { seed: (seed | 1) & 0xFF_FFFF }}

    pub fn next(&mut self) -> u32 {
        let result = Self::generate(self.seed);
        self.seed = result.0;

        return result.1;
    }

    /// Returns a tuple with (new_seed, random_number)
    pub fn generate(seed: u32) -> (u32, u32) {
        let temp = MAGIC_NUMBER.wrapping_mul(seed).wrapping_add(1);
        let new_seed = MAGIC_NUMBER.wrapping_mul(temp).wrapping_add(1);

        // Why or-ing with 1? That causes every number to be odd… (but it's correct)
        let random_number = ((temp >> 16) | new_seed & 0xFFFF_0000) & 0xFF_FFFF | 1;

        (new_seed, random_number)
    }
}

// =============================================================================

/// This RNG is used for most stuff, like AI or dungeon generation
pub struct PMD1Primary {
    seed: u32,
}

impl PMD1Primary {
    pub fn new() -> Self {Self { seed: 0 }}

    // Prevent seed from being 0
    pub fn new_seeded(seed: u32) -> Self {Self { seed: seed | 1 }}

    pub fn next(&mut self) -> u32 {
        let result = Self::generate(self.seed);
        self.seed = result.0;

        return result.1;
    }

    /// Returns a tuple with (new_seed, random_number)
    pub fn generate(seed: u32) -> (u32, u32) {
        let new_seed = (MAGIC_NUMBER.wrapping_mul(seed).wrapping_add(1)) & 0xFFFF_FFFF;
        let random_number = new_seed >> 16;

        (new_seed, random_number)
    }
}
